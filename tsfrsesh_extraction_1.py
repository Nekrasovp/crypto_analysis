#!/usr/bin/env python
# coding: utf-8
# # Cross exchange forecast and anomaly detection with tsfresh
# So we start from importing, from influxdb we import DataFrameClient to get response as pandas df.


import pandas as pd
# import matplotlib.pylab as plt
from influxdb import DataFrameClient
# For tsfresh features extraction parameters we want to use dict with settings or builtin minimal parameters for quick tests of our setup.
from tsfresh import defaults, extract_features
from tsfresh.feature_extraction import ComprehensiveFCParameters
from tsfresh.feature_extraction import MinimalFCParameters

from tsfresh.utilities.dataframe_functions import make_forecasting_frame
from tsfresh.utilities.dataframe_functions import impute

#from sklearn.ensemble import AdaBoostRegressor

# ## Define influxdb query function
# Get ticker data from our db. With DataFrameClient we easily get pd as return.
def get_ticker_data(exchange, symbol):
    '''Get an price data for past hour from the storage
    and return it as pandas dataframe.'''
    client = DataFrameClient('18.182.117.179',8086,'nightly','','')
    myquery = """SELECT last("ask") AS "ask", last("bid") AS "bid" 
                     FROM "TKG"."autogen"."ticker_data" 
                     WHERE time > now() - 61m
                     AND time < now() - 1m
                     AND "exchange"='{}' 
                     AND "ticker"='{}' 
                     GROUP BY time(1s) 
                     FILL(previous)""".format(exchange, symbol) # 'binance', 'BTC/USDT'
    raw_data = client.query(myquery)
    ticker_data = pd.DataFrame(raw_data['ticker_data'])
    return ticker_data

def main():
    # ## Pull data to construct derivative
    # Lets define our settings.
    exchange1 = 'binance'
    exchange2 = 'kucoin'
    symbol = 'BTC/USDT'
    # And call our function to get data
    btc_usdt_binance = get_ticker_data(exchange1,symbol)
    btc_usdt_kucoin = get_ticker_data(exchange2, symbol)
    # Now letts build a new table with mixed data.
    mixed_data = pd.concat([btc_usdt_binance, btc_usdt_kucoin], axis=1).dropna()
    mixed_data.columns = ['binance_ask','binance_bid','kucoin_ask','kucoin_bid']

    mixed_data['bin_ask_kuc_bid'] = mixed_data['binance_ask'] - mixed_data['kucoin_bid']
    mixed_data['bin_bid_kuc_ask'] = mixed_data['binance_bid'] - mixed_data['kucoin_ask']
    mixed_data['mean'] = (mixed_data['bin_ask_kuc_bid'] + mixed_data['bin_bid_kuc_ask'])/2
    #mixed_data['mean_ma'] = mixed_data['mean'].rolling(100).mean() # Count mean with rolling window
    #mixed_data['mmdiff'] = mixed_data['mean_ma'] - mixed_data['mean'] # Diff between ma and current mean amount

    # plt.figure(figsize=(15, 6))
    # plt.plot(mixed_data['mean'])
    # plt.show()

    # So, we have a time series and want to construct a time series model that is able to predict the next data points.
    # To do that, we have to construct a feature matrix by calculating the features for sub time series (see the forecasting section in the tsfresh documentation).

    x = mixed_data['mean']

    df = pd.DataFrame(x)

    df.reset_index(inplace=True)
    df.columns = ["time", "value"]
    df["kind"] = "a"
    df["id"] = 1

    # Takes a singular time series from mixed_data for mean column and constructs a DataFrame df_shift and target vector y that can be used for a time series forecasting task.
    df_shift, y = make_forecasting_frame(x, kind="price", max_timeshift=10, rolling_direction=1)

    # df_shift is ready to be passed into the feature extraction process in tsfresh
    df_shift.info()

    # Extract_features take next parameters:
    # timeseries_container,
    # default_fc_parameters=None,
    # kind_to_fc_parameters=None,
    # column_id=None,
    # column_sort=None,
    # column_kind=None,
    # column_value=None,
    # chunksize=defaults.CHUNKSIZE,
    # n_jobs=defaults.N_PROCESSES,
    # show_warnings=defaults.SHOW_WARNINGS,
    # disable_progressbar=defaults.DISABLE_PROGRESSBAR,
    # impute_function=defaults.IMPUTE_FUNCTION,
    # profile=defaults.PROFILING,
    # profiling_filename=defaults.PROFILING_FILENAME,
    # profiling_sorting=defaults.PROFILING_SORTING,
    # distributor=None

    # The default_fc_parameters is expected to be a dictionary, which maps feature calculator names.
    settings = MinimalFCParameters()
    # Define which features you wanna to extract before calling a function.
    fc_parameters = {
        "length": None,
    }
    # TODO it doesnot work, looks like smsn with multithreading
    X = extract_features(df_shift,
                         column_id="id",
                         column_sort="time",
                         column_value="value",
                         impute_function=impute,
                         default_fc_parameters=settings,
                         show_warnings=False)

    X.info()

if __name__ == '__main__':
    main()